package com.planetbn.spawnerdisable.MainEvent;

import java.util.List;

import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.BlockPlaceEvent;
import org.bukkit.inventory.ItemStack;

import com.planetbn.spawnerdisable.Main;

/**
 * Copyright © PlanetBn. All Rights Reserved.
 * Please do not edit this plugins without any permissions!
 */

public class PlaceSpawner implements Listener {

	public Main plugin;

	public PlaceSpawner(Main plugin) {
		this.plugin = plugin;
	}

	@EventHandler
	public void onSpawnerPlace(BlockPlaceEvent e) {
		Player p = e.getPlayer();
		ItemStack item = p.getInventory().getItemInHand();

		List<String> blworld = plugin.getConfig().getStringList("Disable-On");
		String world = p.getWorld().getName();

		if(item.getType() == Material.MOB_SPAWNER) {
			if(plugin.getConfig().getBoolean("Place-Enable")) {
				if(blworld.contains(world)) {
					e.setCancelled(true);
					p.sendMessage(plugin.getConfig().getString("Place-Msg").replace("&", "§"));
				} else {
					e.setCancelled(false);
				}
			} else {
				e.setCancelled(false);
			}
		}
	}

}



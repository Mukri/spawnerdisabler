package com.planetbn.spawnerdisable.MainEvent;

import java.util.List;

import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.entity.CreatureSpawnEvent;

import com.planetbn.spawnerdisable.Main;

/**
 * Copyright © PlanetBn. All Rights Reserved.
 * Please do not edit this plugins without any permissions!
 */

public class MobSpawning implements Listener {
	
	public Main plugin;
	
	public MobSpawning(Main plugin) {
		this.plugin = plugin;
	}
	
	@EventHandler
	public void onMobSpawnFromSpawner(CreatureSpawnEvent e) {
		List<String> blworld = plugin.getConfig().getStringList("Disable-On");
		String world = e.getLocation().getWorld().getName();
		
		if(blworld.contains(world)) {
			if(e.getSpawnReason() == CreatureSpawnEvent.SpawnReason.SPAWNER) {
				e.setCancelled(true);
			}
		}
	}

}



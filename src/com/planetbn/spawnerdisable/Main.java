package com.planetbn.spawnerdisable;

import org.bukkit.command.Command;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.planetbn.spawnerdisable.MainEvent.MobSpawning;
import com.planetbn.spawnerdisable.MainEvent.PlaceSpawner;

/**
 * Copyright © PlanetBn. All Rights Reserved.
 * Please do not edit this plugins without any permissions!
 */

public class Main extends JavaPlugin implements Listener {
	
	public void onEnable() {
		getLogger().info(">> SpawnerDisable is enable.");
		saveDefaultConfig();
		listen();
	}
	
	public void listen() {
		getServer().getPluginManager().registerEvents(new MobSpawning(this), this);
		getServer().getPluginManager().registerEvents(new PlaceSpawner(this), this);
	}
	
	@Override
	public boolean onCommand(CommandSender sender, Command cmd,
			String label, String[] args) {
		
		if(!(sender instanceof Player)) {
			sender.sendMessage("Players only.");
			return false;
		}
		
		Player p = (Player) sender;
		if(cmd.getName().equalsIgnoreCase("sd")) {
			if(p.hasPermission("spawnerdisabler.reload")) {
				reloadConfig();
				p.sendMessage("§c§lSPAWNERDISABLER RELOADED");
			} else {
				p.sendMessage("§cNo permissions.");
			}
		}
		
		return true;
	}

}


